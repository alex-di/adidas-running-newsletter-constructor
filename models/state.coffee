mongoose = require "mongoose"
Schema = mongoose.Schema

State = new Schema
  id: String
  cid: String
  theme: String
  articles: {}
  events: {}
  runs: {}
  date:
    type: Date
    default: Date.now

module.exports = mongoose.model "State", State