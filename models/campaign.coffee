path = require "path"
config = (require path.join path.dirname(require.main.filename), './config').mailchimp

mongoose = require "mongoose"
Schema = mongoose.Schema
MailChimpAPI = require("mailchimp").MailChimpAPI

Campaign = new Schema
  cid: String
  scheduled:
    type: Boolean
    default: false
  theme: String
  html: String

Campaign.pre "save", (next) ->

  self = this
  opts = config.options
  opts.subject = self.theme
  if !self.cid?
    console.log "Lets create campaign"
    api = self.getApi()

    api.call "campaigns", "create",
      type: "regular",
      options: opts
      content:
        html: self.html

    , (e, data) ->
      if e?
        next(e)
        return
      self.cid = data.id
      next()

  else
    next()

Campaign.methods.getApi = ->
  try
    api = new MailChimpAPI(config.api_key,
      version: "2.0"
    )
    return api
  catch error
    console.log error.message

Campaign.methods.sendTest = (email) ->
  api = this.getApi()
  api.call "campaigns", "send-test", {cid: this.cid, test_emails: email.split ","}, (e, data) ->
    console.log e if e?
    console.log "send-test", data

Campaign.methods.approve = (time) ->

  self = this
  api = self.getApi()
  api.call "campaigns", "schedule", {cid: self.cid, schedule_time: time}, (e, data) ->
    console.log e if e?
    if data.complete? && data.complete
      self.scheduled = true
      self.save()

Campaign.pre "remove", (next) ->
  self = this
  api = self.getApi()
  api.call "campaigns", "delete", {cid: self.cid}, (e, data) ->
    console.log e if e?
    console.log "delete campaign", data
    next()



module.exports = mongoose.model "Campaign", Campaign


