http = require "http"
express = require "express"
moment = require "moment"
path = require "path"
bp = require "body-parser"
fs = require "fs"
bb = require "connect-busboy"
jade = require "jade"
gm = require "gm"
md5 = require "MD5"
config = require "./config"
mongoose = require "mongoose"
.connect config.db
State = require "./models/state"
Campaign = require "./models/campaign"
extend = require "node-extend"


passport = require "passport"
sess = require "express-session"
Strategy = require('passport-http').BasicStrategy;

passport.use new Strategy (user, pass, done) ->
  if user + ":" + pass is "dan:newsletter"
    done(null, "admin")
  else
    done(null, false)

passport.serializeUser (user, done) ->
  done(null, "admin")
passport.deserializeUser (user, done) ->
  done(null, "admin")


app = express()
app.set 'port', process.env.PORT || 4055
app.set('trust proxy', true);
app.set('view engine', 'jade')
.use express.static path.join path.dirname(require.main.filename), './public'

app.use sess({secret: "sss"})
app.use passport.initialize()
app.use passport.session()
app.use passport.authenticate('basic')

app.use(bp.urlencoded({ extended: false, limit: "2mb" }))

app.use(bp.json())
app.use bb()

app.set('views', path.join(__dirname, 'views'));

app.get '/', (req, res) ->
  res.render "index"


render = jade.compileFile(path.join(__dirname, "./views/mail.jade"))
app.post "/", (req, res) ->
  data = JSON.parse req.body.dt
  data.mediaRoot = data.base + "upload/"

  counter = 0

  check = ->
    if --counter is 0
      console.log "Lets put out this shit", data

      res.json
        html: render data

  cropresize = (name, dims, cb) ->
    gm name
    .options({imageMagick: true})
    .size (e, d) ->
        if e
          console.log e
          cb name if cb
        else
          co = dims.width / dims.height
          if d.width / d.height > co
            this.resize null, dims.height
            .crop dims.width, dims.height, (dims.height / d.height * d.width  - dims.width) / 2, 0

          else
            this.resize dims.width
            .crop dims.width, dims.height, 0, (dims.width / d.width * d.height - dims.height) / 2

          fname =  md5(Date.now() + name) + ".jpg"
          iPath = path.join __dirname, "public/upload", fname
          this.write iPath, (e) ->
            console.log e if e?
            cb(fname) if cb?





  for i, itm of data.articles
    ++counter

    do (itm) ->
      image = path.join(__dirname, "public/upload", itm.image)

      cropresize image, {width: 311, height: 208}, (fname) ->
        itm.image = fname
        check()

  for i, itm of data.events
    ++counter
    do (itm) ->

      image = path.join(__dirname, "public/upload", itm.image)

      cropresize image, {width: 33, height: 22}, (fname) ->
        itm.image = fname
        check()

  for i, itm of data.runs
    ++counter
    do (itm) ->

      image = path.join(__dirname, "public/upload", itm.image)

      cropresize image, {width: 311, height: 208}, (fname) ->
        itm.image = fname
        check()

app.post "/files", (req, res) ->
  req.pipe req.busboy
  req.busboy.on 'file', (fieldname, file, filename) ->
    saveTo = path.join(__dirname, "public/upload", filename);
    file.pipe(fs.createWriteStream(saveTo));
    res.json
      path: filename

app.get "/test", (req, res) ->
  res.render "mail",
    {
      "media": "http://energy.adidas-running.ru/img/",
      "articles": [
        {
          "title": "московский марафон. как это было",
          "description": "Долгожданный беговой праздник столицы — Московский Марафон. О том, каким он запомнился беговому сообществу, читай в статье.",
          "image": "http://adidas-running.ru/images/mailing/140925/mm.jpg",
          "link": "http://adidas-running.ru/articles/moskovskij-marafon-2014",
          "date": "23 сентября"
        },
        {
          "title": "блог сергея зырянова. герой 10 забегов подряд",
          "description": "Сергей Зырянов, самая яркая фигура бегового сезона 2014 года, рассказывает о борьбе на ММ и о подготовке к юбилейному старту.",
          "image": "http://adidas-running.ru/images/mailing/140925/zyr.jpg",
          "link": "http://adidas-running.ru/articles/blog-zyrjanova",
          "date": "22 сентября"
        },
        {
          "title": "42 марафонца. история подготовки в runclub>",
          "description": "42 участника boost runclub> рассказывают о том, как они готовили себя к марафонской дистанции.",
          "image": "http://adidas-running.ru/images/mailing/140925/42.jpg",
          "link": "http://adidas-running.ru/articles/42-marafonca",
          "date": "20 сентября"
        },
        {
          "title": "по кругу. музыка для тренировки на ускорения",
          "description": "Сегодня хочется перенести тебя в Калифорнию, в 80-е и 90-е, когда продвинутая молодежь ворочала гитары в родительском гараже.",
          "image": "http://adidas-running.ru/images/mailing/140925/music.jpg",
          "link": "http://adidas-running.ru/articles/po-krugu-music",
          "date": "17 сентября"
        }
      ],
      "events": [
        {
          "title": "warsaw marathon",
          "description": "В последние выходные сентября пробежать 42,2 км можно будет в европейской столице — Варшаве.",
          "link": "http://adidas-running.ru/events/warsaw-marathon",
          "date":

            "day": 28,
            "month": "сентября"

          "flag": "http://adidas-running.ru/images/mailing/flags/poland.png"
        }
      ],
      "runs": [
      ]

    }


app.get "/load/:id", (req, res) ->
  State.findOne
    id: req.params.id
  , (e, state) ->
      console.log e if e?
      unless state?
        res.status 404
        .end()
      else
        res.json state
        console.log state

app.post "/save/:id", (req, res) ->
  data = JSON.parse req.body.dt
  State.findOne
    id: req.params.id
  , (e, state) ->

      state.theme = data.theme
      state.articles = data.articles if data.articles
      state.runs = data.runs if data.runs
      state.events = data.events if data.events
      state.save (e) ->
        console.log state
        res.status 200
        .end()

app.post "/save", (req, res) ->
  data = JSON.parse req.body.dt
  state = new State data

  state.id = md5 state.theme + Date.now()

  state.save (e) ->
    console.log e if e
    res.json
      id: state.id


app.post "/campaign/create", (req, res) ->
  campaign = new Campaign
    html: req.body.html
    theme: req.body.theme


  campaign.save (e) ->
    console.log e if e?
    res.json campaign

app.post "/campaign/test", (req, res) ->
  Campaign.findOne {cid: req.body.cid}, (e, campaign) ->
    console.log e if e?
    campaign.sendTest req.body.email
    res.status 200
    .end()

app.post "/campaign/remove", (req, res) ->
  Campaign.findOne { cid:req.body.cid }, (e, campaign) ->
    campaign.remove ->
      res.status 200
      .end()

app.post "/campaign/approve", (req, res) ->
  Campaign.findOne { cid:req.body.cid }, (e, campaign) ->
    f = "YYYY-MM-DD HH:mm:ss"
    time = moment(req.body.time, f).subtract(4, "hours").format(f)
    console.log time
    campaign.approve(time)
    res.status 200
    .end()


server = http.createServer app
.listen app.get 'port'
