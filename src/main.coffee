Form = require "./form-controller"



app (c) ->
  form = new Form c
  c.f = form
  c.load = c.f.loadData
  riot.route (path) ->
    console.log "Path:", path
    path.replace "http://", ""

    slice = path.slice path.indexOf("#/") + 2

    if slice.length
      c.load slice

app.on "ready", (c) ->
  path = location.hash

  slice = path.slice path.indexOf("#/") + 2

  if slice.length
    c.load slice

window.onload = ->
  app
    root: $("body #wrap")
