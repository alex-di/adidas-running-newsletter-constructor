Notification = require "./notification"

module.exports = (theme, text) ->
  self = riot.observable @
  console.log theme

  email = false

  self.askEmail = () ->
    while !email && !/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test email
      email = prompt "Укажите email, на который нужно отправить тестовое письмо"
      break if !email?

  self.sendEmail = () ->
    $.post "/campaign/test", {email: email, cid: self.d.cid}, (res) ->
      return true


  self.removeCampaign = () ->
    $.post "/campaign/remove", {cid: self.d.cid}, (res) ->
      console.log "removed"

  self.approveCampaign = (time) ->
    $.post "/campaign/approve", {cid: self.d.cid, time: time}, (res) ->
      new Notification "Notification", "Рассылка успешно запланирована"




  $.post "/campaign/create", {html: text, theme: theme}, (res) ->

    self.d = res
    self.trigger "ready", self.d

    self.askEmail()

    if !email?
      email = false
      self.removeCampaign()

    else
      if self.sendEmail()
        if confirm "Вам отправлено тестовое письмо. Нажмите `OK` если всё хорошо или `Отмена` если нужно внести поправки"
          time = prompt "Укажите дату и время рассылки в формате 2014-12-31 00:00:00"
          unless time?
            self.removeCampaign()

          else
            self.approveCampaign(time)

        else
          self.removeCampaign()

      else
        self.removeCampaign()


  return self