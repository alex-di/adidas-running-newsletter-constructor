Notification = require "./notification"
Campaign = require "./campaign"

fileUpload = (file) ->
  self = riot.observable @

  f = new FormData()
  f.append file.name, file

  x = new XMLHttpRequest()
  x.open "post", '/files', true
  x.upload.onprogress = (e) ->
    if e.lengthComputable
      p = e.loaded / e.total
      console.log p * 100 + "%"

  x.onload = ->
    self.trigger "done", JSON.parse @response

  x.send f


timer = null
presave = (r)  ->
  vals = r.find("form").formParams()
  if vals.id?
    console.log timer

    clearTimeout timer if timer?

    timer = setTimeout ->
      saveState(r)
    , 5000

saveState = (r)  ->
  vals = r.find("form").formParams()
  $.post "/save/" + vals.id, {dt: JSON.stringify vals}, (eData) ->
    console.log "saved"


module.exports = (c) ->
  r = $("<div></div>", {id: "form"})

  self = riot.observable this
  c.form = self
  self.el = r

  addArticle = (data) ->
    rData = {num: $(".articles .item").length }
    $.extend rData, data if data?

    r.find ".articles"
    .append riot.render $("#template-article").html(), rData
      .find "input, textarea"
        .on "change", ->
            presave(r)

  addEvent = (data)  ->
    rData = {num: $(".events .item").length, date: {} }
    $.extend rData, data if data?


    r.find ".events"
    .append riot.render $("#template-event").html(), rData
      .find "input, textarea"
        .on "change", ->
            presave(r)

  addRun = (data)->
    rData = {num: $(".runs .item").length }
    $.extend rData, data if data?

    r.find ".runs"
    .append riot.render $("#template-run").html(), rData
      .find "input, textarea"
        .on "change", ->
            presave(r)

  self.loadData = (id)->
    $.get "/load/" + id, (res) ->
      r.find("input[name='id']").val res.id
      r.find("input[name='cid']").val res.cid

      r.find("input[name='theme']").val res.theme

      for i, itm of res.articles
        addArticle itm

      for i, itm of res.events
        addEvent itm

      for i, itm of res.runs
        addRun itm

    .error (e) ->
        new Notification "Error", e.status + ", " + e.statusText


  r.html riot.render $("#template-form").html(), {base: location.origin}

  c.root.append r

  r.on "click", ".articles-add button", addArticle
  r.on "click", ".events-add button", addEvent
  r.on "click", ".runs-add button", addRun

  r.on "submit", "form", ->
    form = @

    l = 0

    check = ->
      if --l is 0
        return true
      else return false


    $("input[type='file']").each ->
      input = @

      ++l
      if @files.length > 0
        (new fileUpload @files[0], @name)
        .on "done", (res) ->
            $(input).next().val res.path

            if check()
              data = $(form).formParams()
              $.post "/", {dt: JSON.stringify data}, (eData) ->
                $("#preview").html eData.html
                $("#source").val eData.html

      else
        if check()
          data = $(form).formParams()
          $.post "/", {dt: JSON.stringify data}, (eData) ->
            $("#preview").html eData.html
            $("#source").val eData.html

    return false

  r.on "click", "#source", ->
    $(@).select()

    return false
  r.on "click", "#save", ->
    data = $(r.find "form").formParams()
    console.log data

    if data.id? && data.id != ''
      saveState(r)

    else
      $.post "/save", {dt: JSON.stringify data}, (eData) ->
        r.find("input[name='id']").val(eData.id)
        console.log eData
        r.find(".container .item").remove()
        riot.route "#/" + eData.id


    return false


  r.on "click", "#create-campaign", ->
    html = $("#source").val()
    theme = r.find("input[name='theme']").val()

    unless html? && html != ""
      new Notification "Warning", "Попытка создать пустую кампанию. Сформируйте кампанию прежде чем рассылать."
      return false

    cmp = new Campaign theme, html
    cmp.on "ready", (campaign)->
      r.find("input[name='cid']").val campaign.cid

    return false


  return self