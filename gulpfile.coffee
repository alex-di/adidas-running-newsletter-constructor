gulp = require "gulp"
browserify = require "gulp-browserify"
watch = require "gulp-watch"
plumber = require "gulp-plumber"
coffee = require 'gulp-coffee'
rename = require 'gulp-rename'
sourcemaps = require "gulp-sourcemaps"
stylus = require "gulp-stylus"
concat = require "gulp-concat"

gulp.task "default", ->
  watch "src/*.coffee", ->
    gulp.src "src/main.coffee", { read: false }
    .pipe plumber()
      .pipe browserify
          insertGoals: true
          debug: true
          transform: ["coffeeify"]
          extensions: ['.coffee']

        .pipe rename "main.js"
          .pipe gulp.dest "./public/js"


  watch "src/*.styl", ->
    gulp.src "src/*.styl"
    .pipe plumber()
      .pipe sourcemaps.init()
      .pipe stylus
          compress: true
        .pipe concat "main.css"
        .pipe sourcemaps.write()

        .pipe gulp.dest "./public/css"
